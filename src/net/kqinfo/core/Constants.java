package net.kqinfo.core;

public class Constants {
	
	//登录用户session名称
	public static final String LOGIN_USER = "login_user";
	//验证码session名称
	public static final String RANDOM_CODE_KEY = "captcha";
	//BCRYPT加密轮数
	public static final int BCRYPT_LOG_ROUNDS = 12;
	//默认每页条数
	public static final int DEFAULT_PAGESIZE = 15;
}
