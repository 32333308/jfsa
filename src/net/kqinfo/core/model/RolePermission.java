package net.kqinfo.core.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * tableName
 * 		security_role_permission
 * coloums
 *		`id`,`permission_id`,`role_id`
 */
@SuppressWarnings("serial")
public class RolePermission extends Model<RolePermission>{

	public static final RolePermission dao = new RolePermission();
	
	public Role getRole(){
		return Role.dao.findById(get("role_id"));
	}
	
	public Permission getPermission(){
		return Permission.dao.findById(get("permission_id"));
	}
}
