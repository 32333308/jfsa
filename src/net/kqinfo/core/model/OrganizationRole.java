package net.kqinfo.core.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * tableName
 *		security_organization_role
 * coloums
 * 		`id`,`priority`,`organization_id`,`role_id`
 */
@SuppressWarnings("serial")
public class OrganizationRole extends Model<OrganizationRole>{

	public static final OrganizationRole dao = new OrganizationRole();
}
