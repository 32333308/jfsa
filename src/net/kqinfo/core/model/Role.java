package net.kqinfo.core.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 * tableName
 * 		security_role
 * coloums
 *		`id`,`description`,`name`
 */
@SuppressWarnings("serial")
public class Role extends Model<Role>{

	public static final Role dao = new Role();
	
	public List<RolePermission> getRolePermissions(){
		return RolePermission.dao.find("select * from security_role_permission where role_id = ? order by id asc",get("id"));
	}
}
