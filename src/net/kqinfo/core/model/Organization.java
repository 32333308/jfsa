package net.kqinfo.core.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 * tableName
 *		security_organization
 * coloums
 * 		`id`,`description`,`name`,`parent_id`
 */
@SuppressWarnings("serial")
public class Organization extends Model<Organization>{

	public static final Organization dao = new Organization();
	
	public List<Organization> getChildren() {
		List<Organization> childList = Organization.dao
				.find("select * from security_organization where parent_id = ? order by id asc",
						get("id"));
		return childList;
	}
	
	public Organization getParent(){
		if (get("parent_id")!=null) {
			return Organization.dao.findById(getLong("parent_id"));
		}
		return null;
	}
}
