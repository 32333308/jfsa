package net.kqinfo.core.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 * tableName
 * 		security_module
 * coloums
 *		`id`,`description`,`name`,`priority`,`sn`,`url`,`parent_id`
 */
@SuppressWarnings("serial")
public class Module extends Model<Module> {

	public static final Module dao = new Module();

	public List<Module> getChildren() {
		List<Module> parentMenu = Module.dao
				.find("select * from security_module where parent_id = ? order by priority asc",
						get("id"));
		return parentMenu;
	}
	
	public Module getParent(){
		return Module.dao.findById(get("parent_id"));
	}
	
	public List<Permission> getPermissions(){
		return Permission.dao.find("select * from security_permission where module_id = ? ", get("id"));
	}
}
