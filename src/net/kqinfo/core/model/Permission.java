package net.kqinfo.core.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * tableName
 *		security_permission
 * coloums
 * 		`id`,`description`,`name`,`short_name`,`module_id`
 */
@SuppressWarnings("serial")
public class Permission extends Model<Permission>{

	public static final Permission dao = new Permission();
	
	/**
	 * 操作
	 */
	public final static String PERMISSION_CREATE = "save";
	
	public final static String PERMISSION_READ = "view";
	
	public final static String PERMISSION_UPDATE = "edit";
	
	public final static String PERMISSION_DELETE = "delete";
	
	public Module getModule(){
		return Module.dao.findById(get("module_id"));
	}
}
