package net.kqinfo.core.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * tableName
 * 		security_user_role
 * coloums
 *		`id`,`priority`,`role_id`,`user_id`
 */
@SuppressWarnings("serial")
public class UserRole extends Model<UserRole> {
	
	public static final UserRole dao = new UserRole();
}
