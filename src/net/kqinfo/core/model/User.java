package net.kqinfo.core.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * tableName
 *		security_user
 * coloums
 * 		`id`,`create_time`,`email`,`password`,`phone`,`realname`,`status`,`username`,`org_id`
 */
@SuppressWarnings("serial")
public class User extends Model<User>{
	
	public static final User dao = new User();
	
	public Organization getOrganization(){
		if (get("org_id")==null) {
			return null;
		}
		return Organization.dao.findById(get("org_id"));
	}
}
