package net.kqinfo.core.controller.management;

import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.jfinal.core.Controller;
import com.jfinal.ext.render.DwzRender;

public class CacheManageController extends Controller{
	
	private static final String INDEX = "/WEB-INF/views/management/security/cacheManage/index.html";
	
	@RequiresPermissions("CacheManage:view")
	public void index() {
		render(INDEX);
	}
	
	@RequiresPermissions("CacheManage:edit")
	public void clear() {
//		cacheService.clearAllCache();
		render(DwzRender.success());
	}
}
