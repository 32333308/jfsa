package net.kqinfo.core.controller.management.condition;

import java.util.List;

import net.kqinfo.core.Constants;
import net.kqinfo.core.controller.management.RoleController;

import com.jfinal.aop.PrototypeInterceptor;
import com.jfinal.core.ActionInvocation;

public class RoleCondition extends PrototypeInterceptor {
	
	private RoleController c;
	
	private static String[] order = {"id asc"};
	private int pageNumber;
	private int pageSize;
	private String name;

	@Override
	public void doIntercept(ActionInvocation ai) {
		init(ai);
		saveCondition(ai);
		c.setCond(this);
		ai.invoke();
	}
	
	private void init(ActionInvocation ai){
		c = (RoleController)ai.getController();
		pageNumber = c.getParaToInt("pageNum",1);
		pageSize = c.getParaToInt("numPerPage", Constants.DEFAULT_PAGESIZE);
		name = c.getPara("name");
	}
	
	private void saveCondition(ActionInvocation ai){
		c = (RoleController)ai.getController();
		c.setAttr("name", name);
	}
	
	public void buildSql(StringBuilder sql, List<Object> paras) {
		sql.append(" from security_role where 1=1 ");
		if (name!=null && !name.trim().equals("")) {
			sql.append(" and name like '%").append(name).append("%'");
		}
		sql.append(" order by ").append(order[0]);
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}
}
