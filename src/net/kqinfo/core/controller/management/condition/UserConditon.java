package net.kqinfo.core.controller.management.condition;

import java.util.List;

import net.kqinfo.core.Constants;
import net.kqinfo.core.controller.management.UserController;

import com.jfinal.aop.PrototypeInterceptor;
import com.jfinal.core.ActionInvocation;

public class UserConditon extends PrototypeInterceptor{
	
	private UserController c;
	
	private static String[] order = {"create_time desc , id asc"};
	private int pageNumber;
	private int pageSize;
	private String username;
	private String realname;
	private String status;
	
	private void init(ActionInvocation ai){
		c = (UserController)ai.getController();
		pageNumber = c.getParaToInt("pageNum",1);
		pageSize = c.getParaToInt("numPerPage", Constants.DEFAULT_PAGESIZE);
		username = c.getPara("username");
		realname = c.getPara("realname");
		status = c.getPara("status");
	}
	
	private void saveCondition(ActionInvocation ai){
		c = (UserController)ai.getController();
		c.setAttr("username", username);
		c.setAttr("realname", realname);
		c.setAttr("status", status);
	}

	@Override
	public void doIntercept(ActionInvocation ai) {
		init(ai);
		saveCondition(ai);
		c.setCond(this);
		ai.invoke();
	}
	
	public void buildSql(StringBuilder sql, List<Object> paras) {
		sql.append(" from security_user where 1 = 1");
		if (username!=null && !username.trim().equals("")) {
			sql.append(" and username like '%").append(username).append("%'");
		}
		if (realname!=null && !realname.trim().equals("")) {
			sql.append(" and realname like '%").append(realname).append("%'");
		}
		if (status!=null && !status.trim().equals("")) {
			sql.append(" and status = '").append(status).append("'");
		}
		sql.append(" order by ").append(order[0]);
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

}
