package net.kqinfo.core.controller.management.condition;

import java.util.List;

import net.kqinfo.core.Constants;
import net.kqinfo.core.controller.management.ModuleController;

import com.jfinal.aop.PrototypeInterceptor;
import com.jfinal.core.ActionInvocation;

public class ModuleCondition  extends PrototypeInterceptor{
	
	private ModuleController c;
	
	private static String[] order = {"priority asc"};
	private int pageNumber;
	private int pageSize;
	private int parent_id;
	private String name;

	@Override
	public void doIntercept(ActionInvocation ai) {
		init(ai);
		saveCondition(ai);
		c.setCond(this);
		ai.invoke();
	}

	private void init(ActionInvocation ai){
		c = (ModuleController)ai.getController();
		pageNumber = c.getParaToInt("pageNum",1);
		pageSize = c.getParaToInt("numPerPage", Constants.DEFAULT_PAGESIZE);
		parent_id = c.getParaToInt(0,1);
		name = c.getPara("name");
	}
	
	private void saveCondition(ActionInvocation ai){
		c = (ModuleController)ai.getController();
		c.setAttr("parent_id", parent_id);
		c.setAttr("name", name);
	}
	
	public void buildSql(StringBuilder sql, List<Object> paras) {
		sql.append(" from security_module where parent_id = ").append(parent_id);
		if (name!=null && !name.trim().equals("")) {
			sql.append(" and name like '%").append(name).append("%'");
		}
		sql.append(" order by ").append(order[0]);
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}
}
