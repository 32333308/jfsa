package net.kqinfo.core.validator;

import net.kqinfo.core.model.User;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class LoginValidator extends Validator{

	@Override
	protected void validate(Controller c) {
		validateRequiredString("user.username", "usernameMsg", "用户不存在!");
		validateRequiredString("user.password", "passwordMsg", "账号或密码错误!");
	}

	@Override
	protected void handleError(Controller c) {
		c.keepModel(User.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/login"))
			c.render("add.html");
		else if (actionKey.equals("/blog/update"))
			c.render("edit.html");
	}

}
