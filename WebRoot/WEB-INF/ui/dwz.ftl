<#include "/WEB-INF/ui/alter.ftl"/>
<#include "/WEB-INF/ui/button.ftl"/>
<#include "/WEB-INF/ui/pager.ftl"/>
<#include "/WEB-INF/ui/panel.ftl"/>
<#include "/WEB-INF/ui/tabs.ftl"/>
<#include "/WEB-INF/ui/accordion.ftl"/>
<#include "/WEB-INF/ui/tree.ftl"/>

<#include "/WEB-INF/ui/form.ftl"/>
<#include "/WEB-INF/ui/form_bar.ftl"/>

<#include "/WEB-INF/ui/input.ftl"/>
<#include "/WEB-INF/ui/label.ftl"/>
<#include "/WEB-INF/ui/dl.ftl"/>

<#include "/WEB-INF/ui/layout.ftl"/>
<#include "/WEB-INF/ui/fieldset.ftl"/>
<#include "/WEB-INF/ui/span.ftl"/>


<#include "/WEB-INF/ui/tool_bar.ftl"/>

<#include "/WEB-INF/ui/table.ftl"/>